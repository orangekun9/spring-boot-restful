package com.example.DogRestApi.service;
import com.example.DogRestApi.entitty.Dog;

import java.util.*;

public interface DogService {
    List<Dog> retrieveDogs();
    List<String> retrieveDogBreed();
    String retrieveDogBreedById(Long id);
    List<String> retrieveDogNames();
}
