package com.example.DogRestApi.entitty;

import javax.persistence.*;

@Entity
public class Dog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private String breed;
    private String origin;

    public Dog(String name, String breed, String origin, long id){
        this.name = name;
        this.breed = breed;
        this.origin = origin;
        this.id = id;
    }

    public Dog(String name, String breed){
        this.name = name;
        this.breed = breed;
    }

    public Dog(){}

    public String getName() {
        return name;
    }

    public String getBreed() {
        return breed;
    }

    public String getOrigin() {
        return origin;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setId(long id) {
        this.id = id;
    }
}
